import React, {useEffect} from 'react'
import { Provider, createClient, useQuery , defaultExchanges, subscriptionExchange} from 'urql';
import {useDispatch} from 'react-redux';
import {setMetrics} from '../Features/Metrics/actions';
import LinearProgress from '@material-ui/core/LinearProgress';
import MetricSelector from '../Features/Metrics/MetricSelector';
import MetricGraph from '../Features/Metrics/MetricGraph';
import SubComponent from '../Features/Metrics/SubComponent';
import {SubscriptionClient} from 'subscriptions-transport-ws'
import DisplayActiveMetrics from '../Features/Metrics/DisplayActiveMetrics';

const subscriptionClient = new SubscriptionClient('wss://react.eogresources.com/graphql',{})

const client = createClient({
    url: 'https://react.eogresources.com/graphql',
    exchanges:[
      ...defaultExchanges,
      subscriptionExchange({
        forwardSubscription: operation => subscriptionClient.request(operation)
      }),
    ],
});



const query = `
{
  getMetrics
}
`

const Metrics = () =>{
  const dispatch = useDispatch();

  const [result] = useQuery({
    query
  });

  const { fetching, data, error } = result;
  useEffect(() =>{
    if (data){
      const metricObjs = data.getMetrics.map((metric, index) =>{
        return {id:index ,metricName:metric, on:false}
      })
      dispatch(setMetrics(metricObjs))
    }

    
  },[dispatch, data, error])



  if (fetching)return <LinearProgress />;
  
  return (
  <>
    <MetricSelector />
    <DisplayActiveMetrics/>
    <MetricGraph />
    <SubComponent />
  
  </>
  ) 
}

export default function MetricsContainer() {
    return (
      <Provider value={client}>
        <Metrics/>
      </Provider>
    )
}

