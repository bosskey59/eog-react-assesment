import { reducer as weatherReducer } from '../Features/Weather/reducer';
import metrics from '../Features/Metrics/metricsReducer';
import currentMeas from '../Features/Metrics/currentMeasReducer';


export default {
  weather: weatherReducer,
  metrics,
  currentMeas
};
