export default (state=[], action) =>{
    switch (action.type) {
        case "SET_METRICS":
            return  action.payload
        case "TOGGLE_METRIC":
            const toggled = state.map(metric =>{
                if (metric.id === action.payload){
                    return {...metric, on: !metric.on}
                }
                return metric
            })
            return  [...toggled]
        default:
            return state;
    }
}