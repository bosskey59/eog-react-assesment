import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function SingleMeasurement({meas}) {
  const classes = useStyles();

  return (
    <Grid item xs={2}>
      <Paper className={classes.paper}>
        <h3>{meas.metric}</h3>
        <h1>{meas[meas.metric]}</h1>
      </Paper>
    </Grid>
  )
}
