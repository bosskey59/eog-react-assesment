import React, {useState, useEffect} from 'react'
import { useSelector } from 'react-redux'
import { useQuery } from 'urql';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Label } from 'recharts';
import Grid from '@material-ui/core/Grid';


const query = `
  query($input: [MeasurementQuery]) {
    getMultipleMeasurements(input: $input) {
      measurements{
        metric
        at
        value
        unit
      }
    }
  }
`
const thirtyToNow = new Date(new Date().getTime() - 1800000).getTime()

export default function MetricGraph() {
  const [allMetrics, setMetricData] = useState(null)

  const metrics = useSelector(state => state.metrics)
  const currentMeas = useSelector(state => state.currentMeas)

  const input = metrics.filter(metric=>(metric.on === true)).map(metric=>({metricName:metric.metricName, after:thirtyToNow}))


  const [result] = useQuery({
    query,
    variables: {
      input,
    },
  });
  
  const {data} = result
  
  useEffect(()=>{
    if (!!data){

      const allData = data.getMultipleMeasurements.reduce((allData, obj) => {
        const toLocal = obj.measurements.map(snap =>{
          return{at: new Date(snap.at).toLocaleTimeString(), [snap.metric]:snap.value, unit:snap.unit}
        })
        return {...allData, [obj.measurements[0].metric]:toLocal}
      },{})


      setMetricData(allData)
    }

  },[data, metrics])

  const handleNewMeasurements = () =>{
    if(currentMeas && Object.keys(currentMeas).length !== 0){
      setMetricData(prevState =>{
        let newState = {}
        for(const metric in prevState){
          newState[metric] = [...prevState[metric], currentMeas[metric]]
        }
        return newState
      })
    }
  }

  useEffect(() => {
    handleNewMeasurements()
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentMeas])





  const DataFormater = (number) => {
    if(number > 1000){
      return (number/1000).toString() + 'K';
    }else{
      return number.toString();
    }
  }

  if (!!allMetrics && Object.keys(allMetrics).length !== 0){
    const createLines = ()=>{
      const colors = ["#8884d8", "#82ca9d", "#66abd4","#a166d4", "#d46a66", "#b1d466"]
      let graphComps = []
      const yUnits = []
      for (const metric in allMetrics){
        const unit = allMetrics[metric][0].unit
        if (!yUnits.includes(unit)){
          graphComps.push(
            <YAxis key={graphComps.length} yAxisId={unit} tickFormatter={DataFormater} tickCount={7} type="number" domain={[dataMin => (Math.floor(dataMin/ 100)*100), dataMax => (Math.ceil(dataMax / 100)*100)]}> 
              <Label angle={270} position={"insideLeft"} style={{ textAnchor: 'middle' }}>
                {unit}
              </Label> 
            </YAxis>
          )
          yUnits.push(unit)
        }
        graphComps.push(<Line key={graphComps.length} yAxisId={unit} isAnimationActive={false} type="monotone" dataKey={metric} stroke={colors[graphComps.length]} dot={false} data={allMetrics[metric]}/>)
      }
      return graphComps
    }

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <LineChart width={1200} height={600} margin={{top: 5, right: 30, left: 20, bottom: 5}} >
          <CartesianGrid stroke="#ccc" />
          <XAxis dataKey="at" allowDuplicatedCategory={false}/>
          <Tooltip />       
          {createLines()}
        </LineChart>

      </Grid>
    </div>
  )
  }else{
    return(
     null
    )
  }
}
