import React from 'react';
import { connect, useDispatch } from 'react-redux';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import {toggleMetric} from './actions'
import Grid from '@material-ui/core/Grid';

 function MetricSelector(props) {
  const dispatch = useDispatch();

  const handleChange = (id) =>{
    dispatch(toggleMetric(id))
  }

  return (
    // TODO Center the toggles
    <div> 
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <FormGroup row >
          {props.metrics.map(metric => (
          <FormControlLabel 
            key={metric.id}
            labelPlacement="top"
            control={
              <Switch
                checked={metric.on}
                onChange={()=>handleChange(metric.id)}
                name={metric.metricName}
                color="primary"
              />
            }
            label={metric.metricName}
          />
          ))}
        </FormGroup>
      </Grid>
    </div>
  )
}

const mapStateToProps = (state) =>{
  return {
    metrics:state.metrics
  }
}

export default connect(mapStateToProps)(MetricSelector)