import React, {useState, useEffect} from 'react'
import { useSelector} from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SingleMeasurement from './SingleMeasurement'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  }
}));

export default function DisplayActiveMetrics() {
  const classes = useStyles();
  const currentMeas = useSelector(state => state.currentMeas)
  const [measurementsDisplay, setmeasurementsDisplay] = useState([])
  const metrics = useSelector(state => state.metrics)


  useEffect(() => {
    if (currentMeas && Object.keys(currentMeas).length !== 0){
      const metricsNameArr = metrics.filter(metric=>(metric.on === true)).map(metric=>(metric.metricName))
      const displayData= metricsNameArr.map((metric) =>(currentMeas[metric]))
      setmeasurementsDisplay(displayData)
    }
    
  }, [currentMeas, metrics])

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        {measurementsDisplay.map((meas,i)=> (<SingleMeasurement key={i} meas={meas}/>))}
      </Grid>
    </div>
  )
  
}
