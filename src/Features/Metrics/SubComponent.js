import React, {useEffect, useCallback, useState} from 'react'
import { useSelector, useDispatch} from 'react-redux'
import { useSubscription } from 'urql';
import { currentMeas } from './actions';


export default function SubComponent() {

  const dispatch = useDispatch();
  const stableDispatch = useCallback(dispatch, [])
  const [latestMetrics, setlatestMetrics] = useState({})

  const metricsFromStore = useSelector(state => state.metrics)

  const metricsNameArr = !!metricsFromStore.find(metric=>(metric.on === true))

  const subscribe = `
    subscription{
      newMeasurement{
        metric
        at
        value
        unit
        
      }
    }
  `

  const [{data: newMeasurement}] = useSubscription({
    query: subscribe
  })


  useEffect(() => {
    if (newMeasurement && metricsNameArr){
      const measurement = newMeasurement.newMeasurement
      const singleMeas = {at: new Date(measurement.at).toLocaleTimeString(), [measurement.metric]:measurement.value, unit:measurement.unit , metric: measurement.metric}
      setlatestMetrics(prevState =>{
        if(Object.values(prevState).length > 0 && !Object.values(prevState).every(({at})=> at === singleMeas.at)){
          return {[singleMeas.metric]:singleMeas}
        }
        return {...prevState, [singleMeas.metric]:singleMeas}
      })
      if(Object.keys(latestMetrics).length === 6 ){
        stableDispatch(currentMeas(latestMetrics))
        setlatestMetrics({})
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newMeasurement, stableDispatch, metricsNameArr])


  
  return (
    <>
     {null}
    </>
  )
}
