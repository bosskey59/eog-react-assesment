export const setMetrics = (metrics) =>({type:"SET_METRICS", payload:metrics})

export const toggleMetric = (id) =>({type:"TOGGLE_METRIC", payload:id})

export const currentMeas = (currentMeas) =>({type:"SET_CURRENT", payload:currentMeas})
